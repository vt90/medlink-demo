import React from 'react';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { selectSong } from '../../actions/songs';
import SongPlayBar from '../Songs/PlayBar';
import { GlobalStyle } from './globalStyles';

const Layout = (props) => {
	const {
		children,
	} = props;
	
	return (
		<>
			<GlobalStyle />
			<Container id="content" maxWidth="md">
				{children}
				
				<SongPlayBar />
			</Container>
		</>
	);
}

const mapStateToProps = (state) => ({
	selectedSongInfo: state.songs.selectedSongInfo,
});

const mapDispatchToProps = (dispatch) => ({
	closeSong: () => dispatch(selectSong(null))
})

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
