import Typography from '@material-ui/core/Typography';
import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			backgroundColor: '#f8f8fa',
			border: '1px solid #c8c8ca',
			borderRadius: theme.spacing(1),
			color: '#5e6674',
			padding: theme.spacing(0.5, 1.5),
		},
	};
})
const Label = (props) => {
	const { text, ...rest } = props;
	const classes = useStyles();
	
	return (
		<Typography
			className={classes.root}
			variant="body2"
			{...rest}
		>
			<strong>
				{text}
			</strong>
		</Typography>
	);
}

export default Label;
