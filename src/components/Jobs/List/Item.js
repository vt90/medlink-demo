import React, { useState } from 'react';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Label from '../../common/Label';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			display: 'block',
			marginBottom: theme.spacing(2),
			'& button': {
				paddingBottom: theme.spacing(1),
				paddingTop: theme.spacing(1),
				textTransform: 'none',
				marginRight: theme.spacing(-4),
			},
		},
	}
});


const JobListItem = (props) => {
	const {
		jobInfo,
		onSelectJob,
	} = props;
	
	const [hovered, setHovered] = useState(null);
	
	const classes = useStyles();
	
	return (
		<Card
			className={classes.root}
			onMouseEnter={() => setHovered(true)}
			onMouseLeave={() => setHovered(false)}
			elevation={hovered ? 8 : 1}
		>
			<CardContent>
				<Grid
					container
					spacing={1}
				>
					<Grid
						item xs={12}
						className="flex align-center space-between"
					>
						<Typography
							variant="h5"
							component="h2"
							noWrap
						>
							{jobInfo.name}
						</Typography>
						&nbsp;
						
						<Typography
							variant="caption"
							color="textSecondary"
							component="p"
						>
							ID: {jobInfo.id}
						</Typography>
					</Grid>
					
					<Grid
						item
						container
						xs={12}
						spacing={2}
						alignItems="center"
					>
						<Grid
							item
							xs={12}
							sm={4}
							md={5}
							className="flex"
						>
							<Label
								text={jobInfo.category}
							/>
						</Grid>
						
						<Grid
							item
							xs={12}
							sm={8}
							md={7}
							container
							spacing={2}
						>
							<Grid
								item
								xs={4}
								sm={3}
							>
								<Typography
									variant="caption"
									color="textSecondary"
									gutterBottom
									noWrap
								>
									POSTCODE
								</Typography>
								
								<Typography
									variant="body2"
									color="textSecondary"
									noWrap
								>
									{jobInfo.postCode}
								</Typography>
							</Grid>
							
							<Grid
								item
								xs={8}
								sm={6}
							>
								<Typography
									variant="caption"
									color="textSecondary"
									gutterBottom
									noWrap
								>
									BEGIN
								</Typography>
								
								<Typography
									variant="body2"
									color="textSecondary"
									noWrap
								>
									{moment(jobInfo.beginDate.startDate).format('MMM DD, YYYY')}
									&nbsp;-&nbsp;
									{moment(jobInfo.beginDate.endDate).format('MMM DD, YYYY')}
								</Typography>
							</Grid>
							
							<Grid
								item
								xs={12}
								sm={3}
								className="flex justify-end"
							>
								<Button
									variant="outlined"
									onClick={() => onSelectJob(jobInfo)}
								>
									Details
								</Button>
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</CardContent>
		</Card>
	)
}

export default JobListItem;
