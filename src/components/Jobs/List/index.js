import React from 'react';
import JobListItem from './Item';

const JobList = (props) => {
	const {
		jobInfoList,
		onSelectJob,
	} = props;
	
	return (
		<>
			{
				jobInfoList && jobInfoList.length
					? (
						jobInfoList.map((job) => (
							<JobListItem
								key={job.id}
								jobInfo={job}
								onSelectJob={onSelectJob}
							/>
						))
					)
						: null
			}
		</>
	)
}

export default JobList;
