import React from 'react';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Check from '@material-ui/icons/Check';
import Close from '@material-ui/icons/Close';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Label from '../../common/Label';
import moment from 'moment';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			display: 'flex',
			flexDirection: 'column',
			padding: theme.spacing(2, 4),
			height: `calc(100vh - ${theme.spacing(2 * 2)}px)`,
		},
		header: {
			backgroundColor: theme.palette.background.paper,
		},
		close: {
			marginRight: theme.spacing(-2),
		},
		content: {
			margin: theme.spacing(2, 0),
			maxHeight: 'calc(100vh - 240px)',
			overflowY: 'scroll',
		},
		contact: {
			'& a': {
				color: 'inherit !important',
			},
			'& span': {
				marginLeft: 16,
				marginRight: 16,
				'&:nth-child(1)': {
					marginLeft: 0,
				},
			},
		},
		actions: {
			'& button': {
				color: '#FFFFFF',
				textTransform: 'none',
			},
		},
	}
})
const JobDetails = (props) => {
	const {
		jobInfo,
		onClose,
	} = props;
	const classes = useStyles();
	
	const renderRow = (title, value) => {
		return (
			<>
				<Grid item xs={12} sm={4} md={5} className="flex align-baseline">
					<Typography
						variant="caption"
						color="textSecondary"
						gutterBottom
						noWrap
					>
						{title.toUpperCase()}
					</Typography>
					&nbsp;
					<div className="fill-flex">
						<Divider />
					</div>
				</Grid>
				<Grid item xs={12} sm={8} md={7}>
					<Typography
						variant="body2"
						color="textSecondary"
						noWrap
					>
						{value}
					</Typography>
				</Grid>
			</>
		);
	};
	
	return (
		<div className={classes.root}>
			<div className={classes.header}>
				<div className="flex align-center">
					<div className="fill-flex">
						<Typography
							variant="caption"
							color="textSecondary"
							component="p"
						>
							ID: {jobInfo.id}
						</Typography>
					</div>
					&nbsp;
					<IconButton
						className={classes.close}
						onClick={onClose}
					>
						<Close />
					</IconButton>
				</div>
				
				<Typography
					variant="h5"
					component="h2"
					gutterBottom
					noWrap
				>
					{jobInfo.name}
				</Typography>
				
				<div className="flex">
					<Label
						text={jobInfo.category}
						gutterBottom
					/>
				</div>
			</div>
			
			<div className={`${classes.content} fill-flex`}>
				<Grid container spacing={1}>
					{renderRow('Institution', jobInfo.institution)}
					{renderRow('Location', jobInfo.location)}
					{renderRow('Qualification', jobInfo.qualification)}
					{renderRow('Details', <span className="flex align-center"><Check />&nbsp;{jobInfo.details}</span>)}
					{renderRow('Dates', (
						<span>
							{jobInfo.dates.map((date, index) => (
								<span key={index}>
									{moment(date.date).format('dddd').substr(0, 2)},&nbsp;
									{moment(date.date).format('DD.MM.YYYY')}&nbsp;von&nbsp;
									{date.startHour}&nbsp;-&nbsp;{date.endHour}&nbsp;UHR
									<br />
								</span>
							))}
						</span>
					))}
					
					<Grid item xs={12}>
						<br />
						<Typography
							variant="caption"
							color="textSecondary"
							gutterBottom
							noWrap
						>
							DESCRIPTION
						</Typography>
						
						<br />
						<br />
						
						<Typography
							variant="body2"
							color="textSecondary"
						>
							{jobInfo.description}
						</Typography>
						<br />
					</Grid>
					
					<Grid item xs={12}>
						<br />
						<Typography
							variant="caption"
							color="textSecondary"
							gutterBottom
							noWrap
						>
							CONTACT
						</Typography>
						
						<br />
						<br />
						
						<Typography
							variant="body2"
							color="textSecondary"
							className={`${classes.contact} flex align-center wrap-content`}
						>
							<span>{jobInfo.contact.name}</span>|
							<span><a href={`mailto:${jobInfo.contact.email}`}>{jobInfo.contact.email}</a></span>|
							<span><a href={`tel:${jobInfo.contact.phoneNumber}`}>{jobInfo.contact.phoneNumber}</a></span>
						</Typography>
						<br />
					</Grid>
				</Grid>
			</div>
			
			<div className={classes.actions}>
				<Button
					variant="contained"
					color="primary"
					fullWidth
				>
					Upload Application
				</Button>
			</div>
		</div>
	);
};

export default JobDetails;
