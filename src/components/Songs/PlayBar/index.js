import React, { useEffect, useState, useRef } from 'react';
import {connect} from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import Collapse from '@material-ui/core/Collapse';
import Container from '@material-ui/core/Container';
import Fab from '@material-ui/core/Fab';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ToggleIcon from 'material-ui-toggle-icon';
import Play from '@material-ui/icons/PlayArrow';
import Stop from '@material-ui/icons/Stop';
import useTheme from '@material-ui/core/styles/useTheme';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import makeStyles from '@material-ui/core/styles/makeStyles';
import AudioSpectrum from 'react-audio-spectrum';
// import { PlayButton, VolumeControl, Progress, Icons } from 'react-soundplayer/components';
// import 'react-soundplayer/styles/buttons.css';
// import 'react-soundplayer/styles/cover.css';
// import 'react-soundplayer/styles/icons.css';
// import 'react-soundplayer/styles/volume.css';
import { setPlaying } from '../../../actions/songs';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			backgroundColor: theme.palette.background.paper,
			padding: theme.spacing(2, 4),
			position: 'fixed',
			bottom: 0,
			left: 0,
			width: '100%',
			zIndex: 10,
		},
		avatar: {
			marginRight: theme.spacing(2),
			height: theme.spacing(6),
			width: theme.spacing(6),
			[theme.breakpoints.only('xs')]: {
				height: theme.spacing(6),
				width: theme.spacing(6),
			}
		},
	}
});


const SongPlayBar = (props) => {
	const {
		selectedSongInfo,
		setPlaying,
	} = props;
	
	const [volume] = useState(1);
	const audioRef = useRef();
	const classes = useStyles();
	const theme = useTheme();
	const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
	const isTablet = useMediaQuery(theme.breakpoints.down('sm'));
	const isLaptop = useMediaQuery(theme.breakpoints.down('md'));
	
	let wavesSize = 600;
	
	if (isMobile) {
		wavesSize = 200;
	}
	else if (isTablet) {
		wavesSize = 100;
	}
	else if (isLaptop) {
		wavesSize = 480;
	}
	
	const isPlaying = selectedSongInfo
		&& selectedSongInfo.playing;
	
	
	useEffect(() => {
		if (audioRef && audioRef.current) {
			audioRef.current.volume = volume;
		}
	}, [volume]);
	
	
	return (
		<Collapse
			in={!!selectedSongInfo}
			className={classes.root}
		>
			{
				selectedSongInfo
					? (
						<Container maxWidth="lg">
							{
								isPlaying
									? (
										<audio
											autoPlay
											ref={audioRef}
											src={selectedSongInfo.url}
											id={`audio-song-${selectedSongInfo.id}`}
										/>
									)
									: null
							}
							
							<Grid
								container
								spacing={4}
							>
								<Grid item xs={12} sm={5} md={4} lg={3} className="flex align-center">
									<div className="flex align-center fill-flex">
										<Avatar
											className={classes.avatar}
											src={selectedSongInfo.coverUrl}
											variant="rounded"
										/>
										<div>
											<Typography variant="subtitle2" noWrap>
												{selectedSongInfo.title}
											</Typography>
											
											<Typography variant="body2"  color="textSecondary" noWrap>
												{selectedSongInfo.author}
											</Typography>
										</div>
									</div>
									
									<div className="flex align-center">
										<div>
											<Fab
												color="primary"
												size="small"
												onClick={() => setPlaying(!isPlaying)}
											>
												<ToggleIcon
													on={isPlaying}
													onIcon={<Stop />}
													offIcon={<Play />}
												/>
											</Fab>
										</div>
									</div>
								</Grid>
								
								<Grid item xs={11} sm={7} md={7} lg={8} className="flex align-center">
									<div className="flex justify-center fill-flex" style={{ minHeight: 64 }}>
										{
											isPlaying
												? (
													<AudioSpectrum
														id="audio-canvas"
														height={64}
														width={wavesSize}
														audioId={`audio-song-${selectedSongInfo.id}`}
														capColor={theme.palette.primary.main}
														capHeight={1}
														meterWidth={6}
														meterCount={512}
														meterColor={[
															{stop: 0, color: theme.palette.primary.main},
															{stop: 0.5, color: theme.palette.secondary.main},
															{stop: 1, color: theme.palette.secondary.dark}
														]}
														gap={4}
													/>
												)
												: null
										}
									</div>
									
									<div style={{ marginLeft: 16 }}>
										<Typography variant="body2"  color="textSecondary">
											{Math.floor(selectedSongInfo.lastPosition / 60)}:
											{
												Math.floor(selectedSongInfo.lastPosition % 60) < 10
													? `0${Math.floor(selectedSongInfo.lastPosition % 60)}`
													: Math.floor(selectedSongInfo.lastPosition % 60)
											}
											&nbsp;/&nbsp;
											{Math.floor(selectedSongInfo.duration / 60)}:
											{
												Math.floor(selectedSongInfo.duration % 60) < 10
													? `0${Math.floor(selectedSongInfo.duration % 60)}`
													: Math.floor(selectedSongInfo.duration % 60)
											}
										</Typography>
									</div>
								</Grid>
								
								<Grid item xs={1}>
								
								</Grid>
							</Grid>
						</Container>
					)
					: null
			}
		</Collapse>
	)
};

const mapStateToProps = (state) => ({
	selectedSongInfo: state.songs.selectedSongInfo,
})

const mapDispatchToProps = (dispatch) => ({
	setPlaying: (value) => dispatch(setPlaying(value)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongPlayBar);
