import React from 'react';
import SongListItem from './Item';

const SongList = (props) => {
	const {
		songInfoList,
		onSelectSong,
	} = props;
	
	return (
		<>
			{
				songInfoList && songInfoList.length
					? (
						songInfoList.map((song, index) => (
							<SongListItem
								key={song.id}
								songInfo={song}
								index={index}
								showDivider={index < songInfoList.length - 1}
								onSelectSong={onSelectSong}
							/>
						))
					)
						: null
			}
		</>
	)
}

export default SongList;
