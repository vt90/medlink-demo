import React from 'react';
import { connect } from 'react-redux';
import Avatar from '@material-ui/core/Avatar';
import ButtonBase from '@material-ui/core/ButtonBase';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import More from '@material-ui/icons/MoreHoriz';
import makeStyles from '@material-ui/core/styles/makeStyles';
import useTheme from '@material-ui/core/styles/useTheme';
import ReactWaves from '@dschoon/react-waves';
import { onMediaEnded, setSongLastPosition } from '../../../actions/songs';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			display: 'block',
			lineHeight: 'inherit',
			marginBottom: theme.spacing(2),
			textAlign: 'left',
			width: '100%',
		},
		avatar: {
			marginRight: theme.spacing(2),
			height: theme.spacing(11),
			width: theme.spacing(11),
			[theme.breakpoints.only('xs')]: {
				height: theme.spacing(6),
				width: theme.spacing(6),
			}
		},
		chip: {
			marginRight: theme.spacing(2),
			marginBottom: theme.spacing(1),
		},
		wavesContainer: {
			marginRight: theme.spacing(2),
		},
		waves: {
			padding: 0,
			width: '100%',
		},
	}
});


const SongListItem = (props) => {
	const {
		index,
		songInfo,
		onMediaEnded,
		onSelectSong,
		selectedSongInfo,
		setSongLastPosition,
		showDivider,
	} = props;
	
	const classes = useStyles();
	const theme = useTheme();
	
	const isPlaying = selectedSongInfo
		&& selectedSongInfo.id === songInfo.id
		&& selectedSongInfo.playing
	
	return (
		<ButtonBase
			component="div"
			className={classes.root}
			onClick={() => onSelectSong(songInfo)}
		>
			<Grid
				container
				spacing={4}
				alignItems="center"
			>
				<Grid item xs={12} sm={5} md={3} className="flex align-center">
					<Avatar
						className={classes.avatar}
						src={songInfo.coverUrl}
						variant="rounded"
					/>
					<div>
						<Typography variant="subtitle2">
							<strong>
								{songInfo.title}
							</strong>
						</Typography>
						
						<Typography variant="body2" color="textSecondary">
							{songInfo.author}
						</Typography>
					</div>
				</Grid>
				<Grid item xs={12} sm={7} md={5} className="flex align-center">
					<div className={`${classes.wavesContainer} fill-flex`}>
						<ReactWaves
							audioFile={songInfo.url}
							className={`${classes.waves} 'react-waves'`}
							options={{
								barHeight: 2,
								cursorWidth: 0,
								height: 60,
								hideScrollbar: true,
								interact: false,
								progressColor: '#369e87', //theme.palette.secondary.main,
								responsive: true,
								waveColor: theme.palette.divider,
							}}
							volume={0}
							zoom={0.1}
							playing={isPlaying}
							pos={songInfo.lastPosition}
							onPosChange={(pos, wavesurfer) => {
								if (Math.floor(wavesurfer.getDuration()) === (Math.floor(pos))) {
									onMediaEnded();
								}
								setSongLastPosition(pos, index);
							}}
						/>
					</div>
					
					<Typography variant="body2"  color="textSecondary">
						{Math.floor(songInfo.duration / 60)}:
						{
							Math.floor(songInfo.duration % 60) < 10
								? `0${Math.floor(songInfo.duration % 60)}`
								: Math.floor(songInfo.duration % 60)
						}
					</Typography>
				</Grid>
				<Grid item xs={9} sm={11} md={3} className="flex wrap-content">
					{
						songInfo.tags
						&& songInfo.tags.length
							? (
								songInfo.tags.map((tag) => (
									<Chip
										className={classes.chip}
										key={tag}
										label={tag}
										size="small"
									/>
								))
							)
							: null
					}
				</Grid>
				<Grid item xs={3} sm={1} className="flex justify-end">
					<IconButton
						onClick={(ev) => {
							ev.stopPropagation();
						}}
					>
						<More/>
					</IconButton>
				</Grid>
			</Grid>
			{
				showDivider
					? (
						<Divider />
					)
					: null
			}
		</ButtonBase>
	)
};

const mapStateToProps = (state) => ({
	selectedSongInfo: state.songs.selectedSongInfo,
})

const mapDispatchToProps = (dispatch) => ({
	setSongLastPosition: (value, index) => dispatch(setSongLastPosition(value, index)),
	onMediaEnded: () => dispatch(onMediaEnded()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongListItem);
