import { SONGS_CONSTANTS } from '../constants/songs';

const reducer = {
	[SONGS_CONSTANTS.GET_SONGS_INIT]: (state) => ({ ...state, isLoading: SONGS_CONSTANTS.GET_SONGS_INIT }),
	[SONGS_CONSTANTS.GET_SONGS_SUCCESS]: (state, { payload }) => ({ ...state, isLoading: null, songInfoList: payload }),
	[SONGS_CONSTANTS.GET_SONGS_FAILURE]: (state) => ({ ...state, isLoading: null }),
	[SONGS_CONSTANTS.SET_SONG_PLAYING]: (state, { payload }) => ({
		...state,
		selectedSongInfo: {
			...state.selectedSongInfo,
			lastPosition: payload ? 0 : state.selectedSongInfo.lastPosition,
			playing: payload,
		}
	}),
	[SONGS_CONSTANTS.SELECT_SONG]: (state, { payload }) => {
		let selectedSongInfo = null;
		
		if (payload) {
			const isAlreadyPlaying = state.selectedSongInfo
				&& state.selectedSongInfo.id === payload.id
				&& state.selectedSongInfo.playing;
			
			selectedSongInfo = {
				...payload,
				playing: !isAlreadyPlaying,
			}
		}
		
		return ({
			...state,
			selectedSongInfo,
		});
	},
	[SONGS_CONSTANTS.SET_SONG_LAST_POSITION]: (state, { payload }) => {
		const newSongInfoList = [...state.songInfoList];
		newSongInfoList[payload.index].lastPosition = payload.value;
		
		return {
			...state,
			selectedSongInfo: {
				...state.selectedSongInfo,
				lastPosition: payload.value,
			},
			songInfoList: newSongInfoList,
		};
	},
};


const initialState = {
	isLoading: null,
	songInfoList: [],
	selectedSongInfo: null,
};

export default (state = initialState, action) => {
	return reducer[action.type] ? reducer[action.type](state, action) : state;
};
