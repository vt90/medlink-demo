import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import songs from './songs';
import jobs from './jobs';

const rootReducer = (history) => combineReducers({
	jobs,
	songs,
	router: connectRouter(history),
});

export default rootReducer;
