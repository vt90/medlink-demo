import { JOBS_CONSTANTS } from '../constants/jobs';

const reducer = {
	[JOBS_CONSTANTS.GET_JOBS_INIT]: (state) => ({ ...state, isLoading: JOBS_CONSTANTS.GET_JOBS_INIT }),
	[JOBS_CONSTANTS.GET_JOBS_SUCCESS]: (state, { payload }) => ({ ...state, isLoading: null, jobInfoList: payload }),
	[JOBS_CONSTANTS.GET_JOBS_FAILURE]: (state) => ({ ...state, isLoading: null }),
	[JOBS_CONSTANTS.SELECT_JOB]: (state, { payload }) => ({ ...state, selectedJobInfo: payload }),
};


const initialState = {
	isLoading: null,
	jobInfoList: [],
	selectedJobInfo: null,
};

export default (state = initialState, action) => {
	return reducer[action.type] ? reducer[action.type](state, action) : state;
};
