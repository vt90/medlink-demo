import React, {Suspense} from 'react';
import { Route, Switch } from 'react-router-dom';
import LinearProgress from '@material-ui/core/LinearProgress';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Layout from './components/Layout';
import { getAvailableRoutes } from './routes';

const useStyles = makeStyles((theme) => {
  return {
    root: {
      minHeight: '100vh',
    },
  };
});

function App() {
  const classes = useStyles();
  const routes = getAvailableRoutes();
  
  return (
    <section className={classes.root}>
      <Suspense fallback={<LinearProgress/>}>
        <Layout>
          <Switch>
            {
              routes.map((route, index) => {
                const {path, component,} = route;
                return (<Route key={index} exact={true} path={path} component={component}/>);
              })
            }
          </Switch>
        </Layout>
      </Suspense>
    </section>
  );
}

export default App;
