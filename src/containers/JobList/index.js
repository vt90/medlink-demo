import Container from '@material-ui/core/Container';
import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import Drawer from '@material-ui/core/Drawer';
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { getJobs, selectJob } from '../../actions/jobs';
import JobDetails from '../../components/Jobs/Details';
import JobList from '../../components/Jobs/List';

const useStyles = makeStyles((theme) => {
	return {
		root: {
			padding: theme.spacing(3, 1),
		},
		drawer: {
			backgroundColor: theme.palette.background.paper,
			zIndex: 2,
			maxWidth: '100vw',
			width: '100vw',
			[theme.breakpoints.up('sm')]: {
				maxWidth: '80vw',
				width: '75vw',
			},
			[theme.breakpoints.up('md')]: {
				maxWidth: '41%',
				width: '41%',
			},
		},
	};
});

const Jobs = (props) => {
	const {
		getJobs,
		isLoading,
		jobInfoList,
		selectJob,
		selectedJobInfo,
	} = props;
	
	const classes = useStyles();
	
	useEffect(()  => {
		getJobs();
	}, [getJobs]);
	
	return (
		<>
			{
				isLoading
					? <LinearProgress />
					: null
			}
			
			<section className={classes.root}>
				<Container maxWidth="lg">
					<Typography
						variant="h4"
						component="h1"
						gutterBottom
					>
						New Jobs
					</Typography>
					
					<JobList
						jobInfoList={jobInfoList}
						onSelectJob={selectJob}
					/>
				</Container>
			</section>
			
			<Drawer
				anchor="right"
				open={!!selectedJobInfo}
				onClose={() => selectJob(null)}
				classes={{
					paper: classes.drawer,
				}}
			>
				{
					selectedJobInfo
						? (
							<JobDetails
								jobInfo={selectedJobInfo}
								onClose={() => selectJob(null)}
							/>
						)
						: null
				}
			</Drawer>
		</>
	);
}

const mapStateToProps = (state) => ({
	...state.jobs,
});

const mapDispatchToProps = (dispatch) => ({
	getJobs: () => dispatch(getJobs()),
	selectJob: (jobInfo) => dispatch(selectJob(jobInfo)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Jobs);
