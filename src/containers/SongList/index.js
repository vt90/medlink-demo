import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import { getSongs, selectSong } from '../../actions/songs';
import SongList from '../../components/Songs/List';
import Typography from '@material-ui/core/Typography';

const Songs = (props) => {
	const {
		getSongs,
		songInfoList,
		selectSong,
	} = props;
	
	useEffect(()  => {
		getSongs();
	}, [getSongs]);
	
	return (
		<>
			<Typography
				component="h1"
				variant="h4"
				gutterBottom
			>
				Media Library Demo
			</Typography>
			<br />
			<SongList
				songInfoList={songInfoList}
				onSelectSong={selectSong}
			/>
		</>
	);
}

const mapStateToProps = (state) => ({
	...state.songs,
});

const mapDispatchToProps = (dispatch) => ({
	getSongs: () => dispatch(getSongs()),
	selectSong: (songInfo) => dispatch(selectSong(songInfo)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Songs);
