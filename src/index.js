import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { BrowserRouter as Router } from 'react-router-dom';

import { createBrowserHistory } from 'history';
import App from './App';
import { configureStore } from './config/redux';
import { theme } from './config/materialUi/theme';
import '../src/styles/global.css';

const history = createBrowserHistory();
const store = configureStore({}, history);

ReactDOM.render(
	<React.StrictMode>
		<ThemeProvider theme={theme}>
			<CssBaseline/>
			<Provider store={store}>
				<Router>
					<App/>
				</Router>
			</Provider>
		</ThemeProvider>
	</React.StrictMode>,
	document.getElementById('root')
);
