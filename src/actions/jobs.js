import { JOBS_CONSTANTS } from '../constants/jobs';

const JOBS = [
	{
		id: 636211,
		name: 'Cardiologist consultant (m/w/d)',
		category: 'Chirurgie',
		postCode: '545300',
		beginDate: {
			startDate: '2020.02.14',
			endDate: '2020.05.14',
		},
		institution: 'UKB Berling',
		location: 'Warener Str. 7, 12683 Berlin, Germany',
		qualification: 'Geratic care',
		details: 'Accomodation',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
		contact: {
			name: 'Max M.',
			email: 'info@medlink.de',
			phoneNumber: '0221 123 321'
		},
		dates: [
			{
				date: '2020.02.14',
				startHour: '20:00',
				endHour: '08:00',
			},
			{
				date: '2020.02.19',
				startHour: '20:00',
				endHour: '08:00',
			},
			{
				date: '2020.04.19',
				startHour: '20:00',
				endHour: '08:00',
			},
		],
	},
	{
		id: 2,
		name: 'Cardiologist consultant (m/w/d)',
		category: 'Chirurgie',
		postCode: '545300',
		beginDate: {
			startDate: '2020.02.14',
			endDate: '2020.05.14',
		},
		institution: 'UKB Berling',
		location: 'Warener Str. 7, 12683 Berlin, Germany',
		qualification: 'Geratic care',
		details: 'Accomodation',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
		contact: {
			name: 'Max M.',
			email: 'info@medlink.de',
			phoneNumber: '0221 123 321'
		},
		dates: [
			{
				date: '2020.02.14',
				startHour: '20:00',
				endHour: '08:00',
			},
			{
				date: '2020.02.19',
				startHour: '20:00',
				endHour: '08:00',
			},
			{
				date: '2020.04.19',
				startHour: '20:00',
				endHour: '08:00',
			},
		],
	}
];

export const getJobs = () => async (dispatch) => {
	try {
		dispatch(getJobsInit());
		
		setTimeout(() => {
			dispatch(getJobsSuccess(JOBS));
		}, 1500);
	}
	catch (e) {
		dispatch(getJobsFailure());
	}
}

export const selectJob = (jobInfo) => ({ type: JOBS_CONSTANTS.SELECT_JOB, payload: jobInfo });

const getJobsInit = () => ({ type: JOBS_CONSTANTS.GET_JOBS_INIT });
const getJobsSuccess = (data) => ({ type: JOBS_CONSTANTS.GET_JOBS_SUCCESS, payload: data });
const getJobsFailure= () => ({ type: JOBS_CONSTANTS.GET_JOBS_FAILURE });
