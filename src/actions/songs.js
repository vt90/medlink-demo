import { SONGS_CONSTANTS } from '../constants/songs';

const SONGS = [
	{
		id: 1,
		coverUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTquiVp7es7enZdspI3OBOJzMb8PU7ndv0DWA&usqp=CAU',
		url: '/music/Kalimba.mp3',
		title: 'Kalimba Dance',
		author: 'Vlad',
		duration: 348,
		lastPosition: 0,
		tags: ['Kalimba', 'Acustic'],
	},
	{
		id: 2,
		coverUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR4aCVnNTNsVAO8M3MgEdcV1vO7aSr-yxCPvA&usqp=CAU',
		url: '/music/Drums.mp3',
		title: 'Drums, Drums & Drums',
		author: 'Tomsa',
		duration: 27,
		lastPosition: 0,
		tags: ['Recording', 'Acustic', 'Electric'],
	}
];

export const getSongs = () => async (dispatch) => {
	try {
		dispatch(getSongsInit());
		
		setTimeout(() => {
			dispatch(getSongsSuccess(SONGS));
		}, 1500);
	}
	catch (e) {
		dispatch(getSongsFailure());
	}
}

export const onMediaEnded = () => async (dispatch) => {
	dispatch(setPlaying(false));
};

export const selectSong = (songInfo) => ({ type: SONGS_CONSTANTS.SELECT_SONG, payload: songInfo });

export const setPlaying= (value) => ({ type: SONGS_CONSTANTS.SET_SONG_PLAYING, payload: value });

export const setSongLastPosition = (value, index) => ({ type: SONGS_CONSTANTS.SET_SONG_LAST_POSITION, payload: { value, index } });

const getSongsInit = () => ({ type: SONGS_CONSTANTS.GET_SONGS_INIT });
const getSongsSuccess = (data) => ({ type: SONGS_CONSTANTS.GET_SONGS_SUCCESS, payload: data });
const getSongsFailure= () => ({ type: SONGS_CONSTANTS.GET_SONGS_FAILURE });
