import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';

const COLORS = {
  PRIMARY: {
    main: '#4b96ac',
  },
  SECONDARY: {
    main: '#00b322',
  },
};

export const theme = responsiveFontSizes(createMuiTheme({
  palette: {
    primary: { ...COLORS.PRIMARY },
    secondary: { ...COLORS.SECONDARY },
    typography: {
      fontFamily: [
        "Gilroy",
        "Poppins",
        "Arial",
        "sans-serif",
      ].join(","),
      useNextVariants: true,
    },
    background: {
      default: '#282828',
      paper: '#202020',
    },
    type: 'dark',
  },
  overrides: {},
}));
