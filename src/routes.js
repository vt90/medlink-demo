//  eslint-disable-next-line
import React from 'react';
import Songs from './containers/SongList';

export const routes = () => {
	return [
		{
			path: '/',
			component: Songs,
			isAvailable: () => true,
		},
	]
};

export const getAvailableRoutes = () => {
	return routes().filter(route => route.isAvailable());
};
